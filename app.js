var express = require('express');
var url = require('url');
var scrape = require('./scrape');
var app = express();

app.use(express.static(__dirname + "/public"));

app.get('/scrape', function (req, res) {
    var parts = url.parse(req.url, true);
    scrape.execute(res, parts);
});

app.listen(8080, function () {
  console.log('App listening on port 8080!');

  // Traer path ingresado por consola
  // var path = process.argv[2];

  // Ejecutar metodo con valor ingresado por consola
  // scrape.executeOnConsole(path);

});

exports = module.exports = app;
