# README #

Simple example of web scraping to count top 10 words used.

### How do I get set up? ###

* Clone the project and move to the folder
* npm install
* node app.js

And thats it, open your browser and navigate to http://localhost:8080/

### Who do I talk to? ###

* Max - [maxzelarayan.com](http://maxzelarayan.com/)
