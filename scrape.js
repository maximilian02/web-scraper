'use strict';
var request = require('request');
var cheerio = require('cheerio');

function scrapeContent(word) {
    var resultObj = {};
    var regex = /(<([^>*]+)>)/ig;
    var result = word.replace(regex, ' ').replace('.', '');

    result = result.replace(/\s\s+/g, ' ').split(' ');
    return processResult(result);
}

function processResult(result) {
    var resultObj = {};
    var exclude = ['', 'las', 'los', 'mis', 'tus'];

    result = result.filter(function(word) {
        var regex = /^[a-zA-Z0-9]*$/g
        return exclude.indexOf(word.toLowerCase()) === -1 && word.length > 2 && regex.test(word);
    });

    for (var i = 0, len = result.length; i < len; i++) {
        if(resultObj.hasOwnProperty(result[i])) {
            resultObj[result[i]] += 1;
        } else {
            resultObj[result[i]] = 1;
        }
    }
    return resultObj;
}

exports.execute = function(res, parts) {

    request(parts.query.path, function(error, response, html){
        if(!error){
            var $ = cheerio.load(html);
            $('script').remove();
            $('style').remove();
            var body = $('body').html();

            var resObj = {
                path: parts.query.path,
                content: scrapeContent(body),
                error: false
            };
            res.json(resObj);
        } else {
          console.log('error', error);
        }
    });

};

exports.executeOnConsole = function(url) {

    request(url, function(error, response, html){
        if(!error){
            // Usar cheerio para aislar el body
            // de donde vamos a sacar el contenido del
            // html
            var $ = cheerio.load(html);
            $('script').remove();
            $('style').remove();
            var body = $('body').html();

            var resObj = { content: scrapeContent(body) };

            console.log(resObj);
        } else {
          console.log('error', error);
        }
    });

};
