(function(){
    'use strict';

    function Scrape() {
        this.regex = /^(ftp|http|https):\/\/[^ "]+$/;
        this.api = "http://localhost:8080/scrape";
    }

    Scrape.prototype = {
        execute: function() {
            var path = $('#url').val();

            if (this.regex.test(path)) {
                $('#error').hide();
                $.ajax({
                    url: this.api,
                    method: 'GET',
                    data: {
                        path: path
                    }
                }).done(function(res) {

                  var result = res.content;
                  var sortable = [];
                  for (var word in result) {
                      sortable.push([word, result[word]])
                  }
                  sortable.sort(function(a, b) {
                    return a[1] - b[1]
                  }).reverse();

                  $('#list').html('');
                  var len = sortable.length > 9 ? 10 : sortable.length;
                  for (var i = 0; i < len; i++) {
                      $('#list').append("<li class='list-group-item'>" + sortable[i][0] + ' (' + sortable[i][1] + ')</li>');
                  }

                  if(res.error) {
                      $('#error').show();
                  } else {
                      $('#error').hide();
                  }
                });
            } else {
                $('#error').show();
            }
        }
    };

    var scrape = new Scrape();

    $('#error').hide();
    $(document).on('click', '#scrape-button', function(event) {
        scrape.execute();
    });

    $(document).on('submit', '#form', function(event) {
        scrape.execute();
        return false;
    });

  })();
